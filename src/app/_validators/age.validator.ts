import { FormControl } from '@angular/forms';

export class AgeValidator {

    static isValid(control: FormControl): any {
        
        var today = new Date();
        var date1 = new Date(control.value);
        var day = date1.getDate();
        var month = date1.getMonth() + 1;
        var year = date1.getFullYear();
        if (today.getFullYear() - year < 18) {
            return {
                "Age must be greater than 18": true
            };
        }
    }

}