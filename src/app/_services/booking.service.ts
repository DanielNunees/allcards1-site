import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Booking } from '../_models/booking';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(private http: HttpClient) { }


  getAll() {
    return this.http.get<Booking[]>(environment.apiUrl + `/booking`);
  }

  getById(id: number) {
    return this.http.get(environment.apiUrl + `/booking` + id);
  }

  register(booking: Booking) {
    return this.http.post(environment.apiUrl + `/booking`, booking);
  }

  update(booking: Booking) {
    return this.http.put(environment.apiUrl + `/booking` + booking.id, booking);
  }

  delete(id: number) {
    return this.http.delete(environment.apiUrl + `/booking` + id);
  }
}
