import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Booking } from '../_models';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  booking: Booking;
  private messageSource = new BehaviorSubject(this.booking);
  currentMessage = this.messageSource.asObservable();

  constructor() { }

  changeMessage(message: Booking) {
    this.messageSource.next(message)
  }


}
