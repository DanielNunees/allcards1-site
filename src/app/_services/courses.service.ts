import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Course } from '../_models';
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(private http: HttpClient) { }


  getAll() {
    return this.http.get<Course[]>(environment.apiUrl + `/courses`);
  }

  getById(id: number) {
    return this.http.get(environment.apiUrl + `/courses/` + id);
  }

  register(course: Course) {
    return this.http.post(environment.apiUrl + `/courses`, course);
  }

  update(course: Course) {
    return this.http.put(environment.apiUrl + `/courses/` + course.id, course);
  }

  delete(id: number) {
    return this.http.delete(environment.apiUrl + `/courses/` + id);
  }
}



