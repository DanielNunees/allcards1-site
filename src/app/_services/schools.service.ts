import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { School } from '../_models';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SchoolsService {

  constructor(private http: HttpClient) { }


  getAll() {
    return this.http.get<School[]>(environment.apiUrl + `/schools`);
  }

  getById(id: number) {
    return this.http.get(environment.apiUrl + `/schools/` + id);
  }

  register(course: School) {
    return this.http.post(environment.apiUrl + `/schools`, course);
  }

  update(course: School) {
    return this.http.put(environment.apiUrl + `/schools/` + course.id, course);
  }

  delete(id: number) {
    return this.http.delete(environment.apiUrl + `/schools/` + id);
  }
}
