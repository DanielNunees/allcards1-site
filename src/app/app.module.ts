import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ArchwizardModule } from 'angular-archwizard';
import { BookingModule } from './booking/booking.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {  HttpClientModule } from '@angular/common/http';
import { NgxStripeModule } from 'ngx-stripe';




@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ArchwizardModule,
    BookingModule,
    AngularFontAwesomeModule,
    HttpClientModule,
    NgxStripeModule.forRoot('pk_test_97iJF4LVH5rpLbjt6SGNoC8x00hvUMtpWI'),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
