import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StripeService, Elements, Element as StripeElement, ElementsOptions } from "ngx-stripe";
import { DataService } from 'src/app/_services/data.service';
import { Customer } from 'src/app/_models/customer';
import { Address, Booking } from 'src/app/_models';
import { BookingService } from 'src/app/_services/booking.service';


@Component({
  selector: 'step4',
  templateUrl: './step4.component.html',
  styleUrls: ['./step4.component.scss']
})
export class Step4Component implements OnInit {
  elements: Elements;
  card: StripeElement;
  cutomer: Customer;
  address: Address;

  // optional parameters
  elementsOptions: ElementsOptions = {
    locale: 'en'
  };

  stripeTest: FormGroup;
  public booking: Booking
  constructor(
    private fb: FormBuilder,
    private stripeService: StripeService,
    private data: DataService,
    private bookingService: BookingService) { }

  ngOnInit() {

    this.data.currentMessage.subscribe(booking => {
      this.booking = booking;
    })
    this.stripeTest = this.fb.group({
      name: ['', [Validators.required]]
    });
    this.stripeService.elements(this.elementsOptions)
      .subscribe(elements => {
        this.elements = elements;
        // Only mount the element the first time
        if (!this.card) {
          this.card = this.elements.create('card', {
            style: {
              base: {
                iconColor: '#666EE8',
                color: '#31325F',
                lineHeight: '40px',
                fontWeight: 300,
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSize: '18px',
                '::placeholder': {
                  color: '#CFD7E0'
                }
              }
            }
          });
          this.card.mount('#card-element');
        }
      });
  }

  get name() { return this.stripeTest.get("name") }

  buy() {
    const name = this.stripeTest.get('name').value;
    this.stripeService
      .createToken(this.card, { name })
      .subscribe(result => {
        if (result.token) {
          console.log(this.booking);
          this.booking.payment_token = result.token;
          this.bookingService.register(this.booking).subscribe(data => {
            console.log(data);
          }, error => {
            console.log(error)
          })
        } else if (result.error) {
          // Error creating the token
          console.log(result.error.message);
        }
      });
  }

}
