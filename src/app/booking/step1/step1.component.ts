import { Component, OnInit } from '@angular/core';
import { Booking } from 'src/app/_models';
import { DataService } from 'src/app/_services/data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss']
})
export class Step1Component implements OnInit {
  //booking: Booking;
  cityCourseForm: FormGroup;
  constructor(private data: DataService, private fb: FormBuilder) { }

  ngOnInit() {
    this.cityCourseForm = this.fb.group({
      city: ["Sydney", Validators.required],
      course: ["", Validators.required],
    })
  }

  get city() {return this.cityCourseForm.get("city").value;}
  get course() {return this.cityCourseForm.get("course").value;}

  enterFirstStep() {
    const booking = new Booking();
    this.data.changeMessage(booking);
  }
}
