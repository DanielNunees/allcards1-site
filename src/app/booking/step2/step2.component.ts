import { Component, OnInit } from '@angular/core';
import { CourseSchedule, Booking } from 'src/app/_models';
import { CourseScheduleService } from 'src/app/_services/course-schedule.service';
import { DataService } from 'src/app/_services/data.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss']
})
export class Step2Component implements OnInit {
  public scheduleForm: FormGroup;
  public page = 1;
  public pageSize = 5
  public collectionSize = 10;
  public collapse: boolean[] = []
  public booking: Booking;
  public valid: boolean = true;
  public schedules: any[];

  constructor(private data: DataService,
    private fb: FormBuilder, private courseScheduleService: CourseScheduleService) { }


  ngOnInit() {
    this.getAllSchedules();

    this.data.currentMessage.subscribe(data => {
      this.booking = data;
    })

    this.scheduleForm = this.fb.group({
      schedule: ["", Validators.required]
    })


  }


  getAllSchedules() {
    this.courseScheduleService.getAll().subscribe(
      data => {
        this.schedules = Object.values(data);
      }, error => {

      })
  }
  updated() {
    if (this.booking.schedule)
      this.valid = false;
  }

  details(i: number) {
    this.collapse[i] = this.collapse[i] ? false : true;
  }

  get schedule() { return this.scheduleForm.get("schedule").value; }

  updateBooking() {
    this.data.changeMessage(this.booking);
  }

}
