import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControlOptions, AsyncValidatorFn, FormControl } from '@angular/forms';
import { DataService } from 'src/app/_services/data.service';
import { Booking } from 'src/app/_models';
import { AgeValidator } from 'src/app/_validators/age.validator';

@Component({
  selector: 'step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.scss']
})
export class Step3Component implements OnInit {
  public customerForm: FormGroup;
  public addressForm: FormGroup;
  public booking: Booking;
  public ageRegex = `(?:0?2[/.-](?:[12][0-9]|0?[1-9])|(?:0?[469]|11)[/.-](?:30|[12][0-9]|0?[1-9])|(?:0?[13578]|1[02])[/.-](?:3[01]|[12][0-9]|0?[1-9]))[/.-][0-9]{4}
  `;
  constructor(private fb: FormBuilder, private data: DataService, ) { }

  ngOnInit() {
    this.customerForm = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      gender: ['', Validators.required],
      birthday: ['', [Validators.required, AgeValidator.isValid]],
      phone: ['', Validators.required],
    });

    this.addressForm = this.fb.group({
      address: ['', Validators.required],
      postcode: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
    });



    this.data.currentMessage.subscribe(booking => {
      this.booking = booking;
    })
  }

  get first_name() { return this.customerForm.get('first_name') }
  get last_name() { return this.customerForm.get('last_name') }
  get email() { return this.customerForm.get('email') }
  get phone() { return this.customerForm.get('phone') }
  get gender() { return this.customerForm.get('gender') }
  get birthday() { return this.customerForm.get('birthday') }

  get address() { return this.addressForm.get('address') }
  get postcode() { return this.addressForm.get('postcode') }
  get city() { return this.addressForm.get('city') }
  get state() { return this.addressForm.get('state') }

  updateBooking() {
    this.booking.address = this.addressForm.value;
    this.booking.customer = this.customerForm.value;
    console.log(this.booking);


    //this.data.changeMessage(this.customerForm)
    if (!this.customerForm.valid)
      return;



  }

}
