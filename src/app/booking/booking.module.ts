import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgbModule, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { ArchwizardModule } from 'angular-archwizard';
import { BookingComponent } from './booking.component';
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { Step4Component } from './step4/step4.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    BookingComponent,
    Step1Component,
    Step2Component,
    Step3Component,
    Step4Component
  ],
  imports: [
    BrowserModule,
    NgbModule,
    ArchwizardModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    NgbPaginationModule,FormsModule
  ],
  providers: [],
})
export class BookingModule { }
