import { Component, OnInit } from '@angular/core';
import { DataService } from '../_services/data.service';
import { CourseScheduleService } from '../_services/course-schedule.service';
import { Booking } from '../_models';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {
  public booking: Booking[][];
  formValidation: boolean = false;
  constructor(private data: DataService, private courseScheduleService: CourseScheduleService) { }

  ngOnInit() {
  }




  }


