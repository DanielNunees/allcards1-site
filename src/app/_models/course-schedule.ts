import { Course } from './courses';

export class CourseSchedule {
    constructor(
        public id: number,
        public course: Course,
        public start_time: Date,
        public end_time: Date,
    ) { }
}