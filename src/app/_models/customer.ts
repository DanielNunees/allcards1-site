
export class Customer {
    constructor(
        public id: number,
        public first_name: string,
        public last_name: string,
        public gender: string,
        public email: string,
        public phone: string,
        public birthday: Date,
    ) { }
}
