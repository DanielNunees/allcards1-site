import { Address } from './address';

export class School {
    constructor(
        public id: number,
        public name: string,
        public email: string,
        public phone: string,
        public description: string,
        public address: Address,
    ) { }
}
