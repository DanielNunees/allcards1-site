import { Address } from './address';
import { Customer } from './customer';
import { Course } from './courses';
import { Schedule } from './schedule';

export class Booking {
    public id: number;
    public address: Address;
    public customer: Customer;
    public course: Course;
    public city: string;
    public schedule: Schedule;
    public payment_token: any;
    public amount: string;
    public description: string;
    constructor(

    ) { }
}
