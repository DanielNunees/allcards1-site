import { Address } from './address';
import { Customer } from './customer';
import { Course } from './courses';

export class Booking {
    public id: number;
    public course_id: number;
    public start_time: Date;
    public end_time: Date;
    public course: Course;
    constructor(

    ) { }
}
