
export class Schedule {
    constructor(
        public id: number,
        public start_time: Date,
        public end_time: Date
    ) { }
}
